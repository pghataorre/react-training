import React, { Component } from "react";
import {NavLink} from 'react-router-dom';

class Header extends Component {
  render() {
    return (
      <header>
        <h1>Task List</h1>
        <nav>
          <ul>
            <li>
              <NavLink to="/">Home</NavLink>
            </li>
            <li>
              <NavLink to="/taskapp">Task App</NavLink>
            </li>
            <li>
              <NavLink to="/tunesapp">Tunes App</NavLink>
            </li>
          </ul>
        </nav>
      </header>
    );
  }
}

export default Header;
