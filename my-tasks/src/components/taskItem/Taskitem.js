import React, { Component } from "react";

class Taskitem extends Component {

  state = {
    displayCompleted: () => {
      return this.props.taskItem.completed ? "completed" : null;
    }
  }
  
  render() {
    const {id, title, completed} = this.props.taskItem;

    return (
      <li className={this.state.displayCompleted()}>
        <label key={id}>
          <input
            type="checkbox"
            id="task-item-complete"
            onChange={this.props.markCompleted.bind(this, id)}
          />
          <span className="task-item-title">{title}</span>

          {completed ? (
            <button 
              onClick={this.props.deleteTaskItem.bind(this, id)} 
              className="delete-item"
              >
              Delete
            </button>
          ) : null}
        </label>
      </li>
    );
  }
}

export default Taskitem;