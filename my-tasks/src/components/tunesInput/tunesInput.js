import React, { Component } from "react";

class Tunesinput extends Component {
  state = {
    trackInput: '',
    trackResults: []
  }

  render() {
    return (
      <div>
        <p>Find a track / artist... type what your looking for.</p>
        <div>
          <label htmlFor="track-search">Search:</label>
          <input
            type="text"
            value={this.props.trackInput}
            onChange={this.props.updateTrackInputeState}
            id="track-search"
            name="track-search"
            placeholder="Artist / Album / Track Name"
            className="text-input"
          />
        </div>
      </div>
    );
  }
}

export default Tunesinput;
