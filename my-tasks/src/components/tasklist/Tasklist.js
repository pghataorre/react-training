import React, { Component } from "react";
import Taskitem from '../taskItem/Taskitem'; 

class Tasklist extends Component {

  render() {
    return this.props.taskList.map((taskItem, key) => (
      <Taskitem
        key={taskItem.id}
        taskItem={taskItem}
        markCompleted={this.props.markCompleted}
        deleteTaskItem={this.props.deleteTaskItem}
      />
    ));
  }
}

export default Tasklist;
