import React, { Component } from 'react';
import Tasklist from "../tasklist/Tasklist";
import Addtask from "../addTask/Addtask";

class Taskapp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tasksTodo: []
    };

    this.getTasksFetch();
  }

  getTasksFetch = () => {
    return fetch('https://jsonplaceholder.typicode.com/posts', {
      mode: 'cors',
      headers: {
        'Access-Control-Allow-Origin': '*'
      }
    })
      .then(response => response.json())
      .then(json => this.setState({ tasksTodo: json }))
      .catch(err => console.log('err =', err));
  }

  getIndexofItem = (arrayItem, id) => {
    return arrayItem.findIndex((taskItem) => { return taskItem.id === id });
  }

  markCompleted = id => {
    const tempTaskItem = this.state.tasksTodo;
    const index = this.getIndexofItem(tempTaskItem, id);

    tempTaskItem[index].completed = !tempTaskItem[index].completed;
    this.setState({ tasksTodo: tempTaskItem });
  };

  deleteTaskItem = id => {
    const tempDeleteTaskItem = this.state.tasksTodo;
    const index = this.getIndexofItem(tempDeleteTaskItem, id);

    tempDeleteTaskItem.splice(index, 1);
    this.setState({
      tasksTodo: tempDeleteTaskItem
    });
  };

  addTaskItem = title => {
    if (title !== '') {
      const tempArr = this.state.tasksTodo;
      const tempObj = {
        id: tempArr.length,
        title,
        completed: false
      };

      tempArr.unshift(tempObj);

      this.setState({
        tasksTodo: tempArr
      });
    }
  }

  render() {
    const taskCount = this.state.tasksTodo.length === null ? 0 : this.state.tasksTodo.length;

    return (
      <div className="todo-app-container">
        <h2>To Do App</h2>
        <Addtask
          addTaskItem={this.addTaskItem}
        />
        <section className="task-list">
          {taskCount > 0 ?
            <ul>

              <Tasklist
                taskList={this.state.tasksTodo}
                markCompleted={this.markCompleted}
                deleteTaskItem={this.deleteTaskItem}
              />
            </ul>
            :
            <h2 className="empty-tasks">You have no tasks currently</h2>
          }
        </section>
      </div>
    );
  }
}

export default Taskapp;
