import React, { Component } from "react";
import Tunesinput from '../tunesInput/tunesInput';
import Tuneslist from '../tunesList/tunesList';

class Tunesapp extends Component {
  state = {
    trackInput: "",
    trackResults: [],
    trackListCount: 0
  };

  updateTrackInputeState = (event) => {
    let trackInputVal = event.target.value;

    if (trackInputVal !== '') {
      this.setInputState(trackInputVal);
      const urlPath = `https://itunes.apple.com/search?term=${trackInputVal}&limit=25`;

      this.getTasksFetch(urlPath)
      .then(response => {
        this.setState({
          trackResults: response,
          trackListCount: response ? response.length : 0
        });
      })
      .catch(err => {
        console.log(err);
      });
    }
  }
  
  getTasksFetch = (url) => {
    return fetch(url, {
      mode: "cors",
      headers: {
        "Access-Control-Allow-Origin": "*"
      }
    })
    .then(response => response.json())
    .then((response) => {
      return response.results;
    })
    .catch(err => console.log("err =", err))
  }

  setInputState = (trackVal) => {
    this.setState({
      trackInput: trackVal
    });
  }

  render() {
    return (
      <div>
        <section className="tunes-app">
          <h2>Tunes App</h2>
          <Tunesinput
            trackInput={this.state.trackInput}
            updateTrackInputeState={this.updateTrackInputeState}
          />
        </section>
        <section className="track-list">
          <Tuneslist
            trackListing={this.state.trackResults}
            trackListCount={this.state.trackListCount}
          />
        </section>
      </div>
    );
  }
}

export default Tunesapp;