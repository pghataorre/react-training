import React, { Component } from "react";
import Trackitem from '../tuneItem/tuneItem'

class Tuneslist extends Component {

  noTracksList = () => this.props.trackListCount
    ? <li><h2>Results : {this.props.trackListCount}</h2></li>
    : <li><h2>Please enter a value in the search field above</h2></li>;

  render() {
    return (
      <div>
        <ul>
          {this.noTracksList()}
          {this.props.trackListing.map((trackItem, key) =>
            <Trackitem trackItem={trackItem} key={key} />
          )}
        </ul>
      </div>
    )
    }
  }

export default Tuneslist;
