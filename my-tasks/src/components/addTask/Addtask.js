import React, { Component } from "react";

class Addtask extends Component {
  state = {
    textContent: ''
  };

  updateInputValue = (event) =>{
    this.setState({
      textContent: event.target.value
    });
  }

  render() {
    return (
      <section className="add-task">
        <input 
          type="text"
          value={this.state.textContent}
          onChange={this.updateInputValue}
          id="add-task-tex"
          placeholder="Add a task here" 
          className="text-input"/>
        
         <button 
          type="button"
          onClick={this.props.addTaskItem.bind(this, this.state.textContent)}
          className="add-task-button">
          Add Task
        </button>
      </section>
    );
  }
}

export default Addtask;