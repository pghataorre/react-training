import React, { Component } from 'react';

class App extends Component {

  render() {
    return (
      <div className="index-container">
        <h2>
          Welcome to my test React Mini App
        </h2>
      </div>
    );
  }
}

export default App;
