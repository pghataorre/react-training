import React, { Component } from "react";

class Tuneitem extends Component {
  render() {

    const { artistName, trackName, artworkUrl60, previewUrl} = this.props.trackItem; 

    return (
      <li key={this.props.key}>
        <span>
          <img src={artworkUrl60} alt={`${artistName} - ${trackName}`} />
        </span>
        <span className="track-title">{`${artistName} - ${trackName}`}</span>
        <audio controls>
          <source src={previewUrl} type="audio/mp4" />
        </audio>
      </li>
    );
  }
}

export default Tuneitem;