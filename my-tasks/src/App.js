import React, {Component} from 'react';
import './App.scss';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Header from './components/header/Header';
import Index from './components/default/index';
import TaskApp from "./components/taskApp/taskApp";
import TunesApp from "./components/tunesApp/tunesApp";
import Error from "./components/error/error";

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Header />
          <Switch>
            <Route path="/" component={Index} exact />
            <Route path="/taskapp" component={TaskApp} />
            <Route path="/taskapp" component={TaskApp} />
            <Route path='/tunesapp' component={TunesApp} />
            <Route component={Error} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
